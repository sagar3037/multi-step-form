## Multi Step Form Example

  1. 4 step form with partial form submit
  2. Used redux to handle state
  3. Progress bar to track the page
  4. Simple validations


## Installation

You must have node/npm installed to run this.
Also download the react-native cli if you don't have it.

```js
cd <Project directory>
npm install

### IOS
npx react-native run-ios

### Android
npx react-native run-android

### Start Metro bundler
npx react-native start

```
