import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Page1 from '../screens/page1';
import Page2 from '../screens/page2';
import Page3 from '../screens/page3';
import Page4 from '../screens/page4';
import Restart from '../screens/restart';

import { connect } from 'react-redux';

const mapStateToProps = state => {
  return state
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const Stack = createStackNavigator();

function App({ propsData }) {
  return (
    <NavigationContainer headerMode={'none'}>
      <Stack.Navigator initialRouteName={'page1'} headerMode={'none'}>
        <Stack.Screen name="page1" component={Page1} />
        <Stack.Screen name="page2" component={Page2} />
        <Stack.Screen name="page3" component={Page3} />
        <Stack.Screen name="page4" component={Page4} />
        <Stack.Screen name="restart" component={Restart} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

class AppContainer extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <App propsData={this.props} />
    )
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)
