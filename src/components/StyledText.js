import React from 'react';
import { Text, StyleSheet } from 'react-native';

const BaseText = (props) => (
    <Text {...props} />
)

export const TextRegular = (props) => (
    <BaseText  {...props} style={[props.style, styles.defaultText]} />
)

const styles = StyleSheet.create({
    defaultText: {
        fontFamily: 'SFProDisplay-Regular'
    }
});