import React from 'react'
import { connect } from 'react-redux';
import { StyleSheet, View, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import AntIcon from 'react-native-vector-icons/AntDesign';
import Colors from '../themes/color';
import * as StyledText from './StyledText';
import { scale } from '../utils/scaling';
import Font from '../themes/fonts';
import { progressUpdate } from '../redux/actions/form';

class AppHeader extends React.Component {

    constructor(props) {
        super(props)
    }

    goBack = () => {
        if (this.props.goBackPage) {
            this.props.progressUpdate(this.props.goBackPage)
        }
        this.props.navigation.goBack();
    }

    render() {
        console.log(this.props)
        const { goBack } = this.props.navigation;
        let { progress } = this.props.form;
        return (
            <>
                <View style={styles.appHeader}>
                    <StatusBar backgroundColor={Colors.lightPrimary} barStyle="light-content" />
                    {(progress != 'Page1') ?
                        <View>
                            <TouchableOpacity onPress={() => { this.goBack() }}>
                                <AntIcon name="arrowleft" style={{ padding: 10 }} size={30} color="white" />
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={styles.header}>
                            <TouchableOpacity>
                                <StyledText.TextRegular style={styles.headerLabel}>Multi Step Form</StyledText.TextRegular>
                            </TouchableOpacity>
                        </View>
                    }
                    {/* <View >
                    </View> */}
                </View>
            </>
        )
    }
}

AppHeader.defaultProps = {
    leftIcon: null,
    backButton: false
}


const mapStateToProps = state => {
    return {
        ...state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch,
        progressUpdate: (payload) => dispatch(progressUpdate(payload)),
    }

}
export default connect(mapStateToProps, mapDispatchToProps)(AppHeader)

const styles = StyleSheet.create({
    appHeader: {
        backgroundColor: Colors.brandPrimary,
    },
    leftIcon: {
        width: 50,
        paddingHorizontal: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        marginBottom: -18,
        marginTop: -15
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
    },
    headerLabel: {
        fontSize: scale(Font.fontSize.large),
        color: Colors.white
    }
})