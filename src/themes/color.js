export default {
	brandPrimary: '#ef6736',
	lightPrimary: '#e26031',
	gray: 'gray',
	white: '#ffffff',
	lightWhite: '#fcfcfc'
};
