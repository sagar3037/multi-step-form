const fontSize = {
	sm: 11,
	sm1: 12,
	med: 14,
	large: 20
};

const Font = { fontSize };
export default Font;
