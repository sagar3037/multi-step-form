import types from './types';
import Api from '../../mocks/api';

const progressUpdate = (payload) => {
	return {
		type: types.progressUpdate,
		payload: payload
	};
};

const submitPartialForm = (data) => (dispatch) => {
	dispatch({ type: types.requestInitiation });
	Api.formService(data.res).then((result) => {
		dispatch({ type: types.submitPartialForm, payload: result });
		dispatch({ type: types.requestCompleted });
		if (result.page === 'Page1') {
			dispatch(progressUpdate('Page2'));
			data.navigation.navigate('page2');
		} else if (result.page === 'Page2') {
			dispatch(progressUpdate('Page3'));
			data.navigation.navigate('page3');
		} else if (result.page === 'Page3') {
			dispatch(progressUpdate('Page4'));
			data.navigation.navigate('page4');
		} else {
			data.navigation.navigate('restart');
		}
	});
};

const resetUserState = () => {
	return {
		type: types.resetUserState
	};
};
export { progressUpdate, submitPartialForm, resetUserState };
