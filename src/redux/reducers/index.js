import userReducer from './user';
import formReducer from './form';

export default reducers = {
    user: userReducer,
    form: formReducer
}