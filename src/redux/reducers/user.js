import types from '../actions/types';

const initialState = {
	name: null,
	age: null,
	gender: null,
	tobaccoUsed: null,
	riskGroupCategorySelected: null,
	loading: false,
	page: null
};

const user = (state = initialState, action) => {
	switch (action.type) {
		case types.submitPartialForm:
			return { ...state, ...action.payload };
		case types.requestInitiation:
			return { ...state, loading: true };
		case types.requestCompleted:
			return { ...state, loading: false };
		case types.resetUserState:
			return initialState;
		default:
			return state;
	}
	return state;
};
export default user;
