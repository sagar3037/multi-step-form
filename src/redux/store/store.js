import { createStore, combineReducers, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import reducers from '../reducers';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}
let middleware = [];
const rootReducer = combineReducers(reducers);
const persistedReducer = persistReducer(persistConfig, rootReducer)
middleware = [ thunk, logger ];
const defaultStore = () => {

    let store = createStore(
        persistedReducer,
        applyMiddleware(...middleware)
    )
    let persistor = persistStore(store)
    return { store, persistor }
}

export default defaultStore;