import Page4 from './page4';
import { connect } from 'react-redux';
import { submitPartialForm } from '../../redux/actions/form';

const mapStateToProps = state => {
	return {
		loading: state.user.loading,
		progress: state.form.progress,
        hobby: state.form.hobby
	};
}

const mapDispatchToProps = dispatch => {
    return {
		submitPartialForm: (data) => dispatch(submitPartialForm(data))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Page4);