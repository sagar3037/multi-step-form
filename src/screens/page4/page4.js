import React, { Component } from 'react';
import { View, Text, KeyboardAvoidingView, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from './style';
import Colors from '../../themes/color';
import ProgressStatus from '../../components/progressBar';
import { isEmpty } from '../../utils/empty-utils';
import RadioButtonGroup from '../../components/RadioButton';
import AppHeader from '../../components/header';

class Page4 extends Component {

    constructor(props) {
        super(props)
        this.state = {
            selectedElementIndex: null
        }
        this.riskGroupCategories = [
            { id: 101, category: 'Low risk Lower returns' },
            { id: 102, category: 'Balanced Risk Moderate returns' },
            { id: 103, category: 'High Risk High Return' }
        ];
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
    }

    onSubmit() {
        this.props.submitPartialForm({ res: { page: 'Page4', riskGroupCategorySelected: this.riskGroupCategories[this.state.selectedElementIndex] }, navigation: this.props.navigation });
    }

    render() {
        const { progress, loading } = this.props;
        return (
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                <AppHeader navigation={this.props.navigation} goBackPage="page3" />
                <View style={{ paddingTop: 25 }}>
                    <ProgressStatus values={['Page1', 'Page2', 'Page3', 'Page4']} status={progress} />
                </View>
                <View style={styles.inner}>
                    <View>
                        <Text style={[styles.questionText, { alignSelf: 'center' }]}>
                            Which best describes your risk appetite?
						</Text>
                        <RadioButtonGroup
                            group={this.riskGroupCategories}
                            radioButtonChecked={(index) => this.setState({ selectedElementIndex: index })}
                            selectedElementIndex={this.state.selectedElementIndex}
                        />
                    </View>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        disabled={isEmpty(this.state.selectedElementIndex)}
                        onPress={this.onSubmit}
                        style={[styles.submitButton, { backgroundColor: (isEmpty(this.state.selectedElementIndex)) ? Colors.gray : Colors.brandPrimary }]}>
                        {loading ? (
                            <ActivityIndicator color={Colors.white} />
                        ) : (
                            <Text style={styles.submitButtonText}>Finish</Text>
                        )}
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Page4;