import { StyleSheet } from 'react-native';
import { verticalScale, scale } from '../../utils/scaling';
import Colors from '../../themes/color';
import Font from '../../themes/fonts';

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.lightWhite
	},
	inner: {
		padding: scale(14)
	},
	questionText: {
		marginTop: verticalScale(20),
		fontSize: scale(Font.fontSize.med),
		alignSelf: 'center',
		color: Colors.brandPrimary
	},
	btnContainer: {
		marginTop: verticalScale(40),
		justifyContent: 'flex-end'
	},
	CheckboxOptionView: {
		paddingTop: verticalScale(5)
	},
	submitButton: {
		width: '55%',
		alignSelf: 'center',
		backgroundColor: Colors.brandPrimary,
		justifyContent: 'center',
		borderRadius: 6,
		height: 50
	},
	submitButtonText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: scale(Font.fontSize.med)
	}
})

export default styles;