import { StyleSheet } from 'react-native';
import { scale } from '../../utils/scaling';
import Colors from '../../themes/color';
import Font from '../../themes/fonts';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	submitButton: {
		width: '70%',
		backgroundColor: Colors.brandPrimary,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 6,
		padding: 20
	},
	submitButtonText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: scale(Font.fontSize.med)
	}
})

export default styles;