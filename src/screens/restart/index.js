import Restart from './restart';
import { connect } from 'react-redux';
import { progressUpdate, resetUserState } from '../../redux/actions/form';

const mapStateToProps = state => {
    return { ...state };
}

const mapDispatchToProps = dispatch => {
    return {
        progressUpdate: (payload) => dispatch(progressUpdate(payload)),
        resetUserState: () => dispatch(resetUserState())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Restart);