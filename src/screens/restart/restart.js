import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './style';

class Restart extends React.Component {

    constructor(props) {
        super(props)
    }

    restartSignup() {
        this.props.progressUpdate('Page1');
        this.props.resetUserState();
        this.props.navigation.pop(6);
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.restartSignup()} style={styles.submitButton}>
                    <Text style={styles.submitButtonText}>Restart</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Restart;