import React, { Component } from 'react';
import { View, ScrollView, Text, KeyboardAvoidingView, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from './style';
import Colors from '../../themes/color';
import ProgressStatus from '../../components/progressBar';
import { isEmpty } from '../../utils/empty-utils';
import TextInputWithInfo from '../../components/TextInputWithInfo';
import CheckboxOption from '../../components/CheckBoxOption';
import AppHeader from '../../components/header';

class Page2 extends Component {

    constructor(props) {
        super(props)
        this.state = {
            age: 10,
            ageError: null,
            checkBoxOptionSelected: null
        }
        this.onAgeChange = this.onAgeChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        const { age, gender } = this.props;
        this.setState({
            age: age,
            checkBoxOptionSelected: gender && gender === 'Male' ? 'option1' : !gender ? null : 'option2'
        });
    }

    validateAgeField(value) {
        if (value > 200) return 'Age is not correct';
        return null;
    }

    onAgeChange(age) {
        this.setState({
            age: age,
            ageError: age > 200 ? 'Age is not correct' : null
        });
    }

    onSubmit() {
        const genderSelected = this.state.checkBoxOptionSelected === 'option1' ? 'Male' : 'Female';
        this.props.submitPartialForm({ res: { page: 'Page2', age: parseInt(this.state.age), gender: genderSelected }, navigation: this.props.navigation });
    }

    render() {
        const { progress, loading } = this.props;
        return (
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                <AppHeader navigation={this.props.navigation} goBackPage="page1" />
                <View style={{ paddingTop: 25 }}>
                    <ProgressStatus values={['Page1', 'Page2', 'Page3', 'Page4']} status={progress} />
                </View>
                <ScrollView style={styles.inner} keyboardShouldPersistTaps="handled">
                    <Text style={styles.questionText}>{'What is your age'}</Text>
                    <View style={styles.textInputWithInfoView}>
                        <TextInputWithInfo
                            onChangeText={this.onAgeChange}
                            val={this.state.age}
                            keyboardType={'numeric'}
                            errorVal={this.state.ageError}
                            whyAskingText={'Your age in years'}
                            textAlign={'center'}
                            autoCorrect={false}
                            autoFocus={true}
                        />
                    </View>
                    <View>
                        <Text style={styles.questionText}>Gender</Text>
                        <CheckboxOption
                            option1={'Male'}
                            option2={'Female'}
                            selectedOption={this.state.checkBoxOptionSelected}
                            onOption1Press={() => this.setState({ checkBoxOptionSelected: 'option1' })}
                            onOption2Press={() => this.setState({ checkBoxOptionSelected: 'option2' })}
                        />
                    </View>
                </ScrollView>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        disabled={(isEmpty(this.state.age) || !isEmpty(this.state.ageError) || isEmpty(this.state.checkBoxOptionSelected))}
                        onPress={this.onSubmit}
                        style={[styles.submitButton, {
                            backgroundColor: ((isEmpty(this.state.age) || !isEmpty(this.state.ageError) || isEmpty(this.state.checkBoxOptionSelected))) ? Colors.gray : Colors.brandPrimary
                        }]}>
                        {loading ? (
                            <ActivityIndicator color={Colors.white} />
                        ) : (
                            <Text style={styles.submitButtonText}>Next</Text>
                        )}
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Page2;