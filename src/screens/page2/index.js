import Page2 from './page2';
import { connect } from 'react-redux';
import { submitPartialForm } from '../../redux/actions/form';

const mapStateToProps = state => {
	return {
		loading: state.user.loading,
		progress: state.form.progress,
		age: state.user.age,
		gender: state.user.gender
	};
}

const mapDispatchToProps = dispatch => {
    return {
		submitPartialForm: (data) => dispatch(submitPartialForm(data))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Page2);