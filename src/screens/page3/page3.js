import React, { Component } from 'react';
import { View, ScrollView, Text, KeyboardAvoidingView, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from './style';
import Colors from '../../themes/color';
import ProgressStatus from '../../components/progressBar';
import { isEmpty } from '../../utils/empty-utils';
import CheckboxOption from '../../components/CheckBoxOption';
import AppHeader from '../../components/header';

class Page3 extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            checkBoxOptionSelected: null
        }
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        const { tobaccoUsed } = this.props;
        this.setState({
            checkBoxOptionSelected: tobaccoUsed && tobaccoUsed === 'Yes' ? 'option1' : !tobaccoUsed ? null : 'option2'
        });
    }

    onSubmit() {
        const tobaccoUsed = this.state.checkBoxOptionSelected === 'option1' ? 'Yes' : 'No';
        this.props.submitPartialForm({ res: { page: 'Page3', tobaccoUsed: tobaccoUsed }, navigation: this.props.navigation });
    }

    render() {
        const { progress, loading } = this.props;

        return (
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                <AppHeader navigation={this.props.navigation} goBackPage="page2" />
                <View style={{ paddingTop: 25 }}>
                    <ProgressStatus values={['Page1', 'Page2', 'Page3', 'Page4']} status={progress} />
                </View>
                <ScrollView style={styles.inner} keyboardShouldPersistTaps="handled">
                    <View style={styles.CheckboxOptionView}>
                        <Text style={[styles.questionText, { alignSelf: 'center' }]}>Are you a tobacco user?</Text>
                        <CheckboxOption
                            option1={'Yes'}
                            option2={'No'}
                            selectedOption={this.state.checkBoxOptionSelected}
                            onOption1Press={() => this.setState({ checkBoxOptionSelected: 'option1' })}
                            onOption2Press={() => this.setState({ checkBoxOptionSelected: 'option2' })}
                        />
                    </View>
                </ScrollView>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        disabled={isEmpty(this.state.checkBoxOptionSelected)}
                        onPress={this.onSubmit}
                        style={[styles.submitButton, { backgroundColor: (isEmpty(this.state.checkBoxOptionSelected)) ? Colors.gray : Colors.brandPrimary }]}>
                        {loading ? (
                            <ActivityIndicator color={Colors.white} />
                        ) : (
                            <Text style={styles.submitButtonText}>Next</Text>
                        )}
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Page3;