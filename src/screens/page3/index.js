import Page3 from './page3';
import { connect } from 'react-redux';
import { submitPartialForm } from '../../redux/actions/form';

const mapStateToProps = state => {
	return {
		loading: state.user.loading,
		progress: state.form.progress,
		tobaccoUsed: state.user.tobaccoUsed
	};
}

const mapDispatchToProps = dispatch => {
    return {
		submitPartialForm: (data) => dispatch(submitPartialForm(data))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Page3);