import { StyleSheet } from 'react-native';
import { verticalScale, scale } from '../../utils/scaling';
import Colors from '../../themes/color';
import Font from '../../themes/fonts';

const styles = StyleSheet.create({
	container: {
		backgroundColor: Colors.lightWhite
	},
	inner: {
		padding: scale(14)
	},
	questionText: {
		marginTop: verticalScale(60),
		fontSize: scale(Font.fontSize.large),
		alignSelf: 'center',
		color: Colors.brandPrimary
	},
	textInputWithInfoView: {
		width: '100%',
		paddingTop: '20%'
	},
	submitButton: {
		width: '55%',
		alignSelf: 'center',
		backgroundColor: Colors.brandPrimary,
		justifyContent: 'center',
		borderRadius: 6,
		height: 50
	},
	submitButtonText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: scale(Font.fontSize.med)
	}
})

export default styles;