import React, { Component } from 'react';
import { View, Text, KeyboardAvoidingView, Platform, TouchableOpacity, ActivityIndicator, ScrollView } from 'react-native';
import styles from './style';
import Colors from '../../themes/color';
import ProgressStatus from '../../components/progressBar';
import { isEmpty } from '../../utils/empty-utils';
import TextInputWithInfo from '../../components/TextInputWithInfo';
import AppHeader from '../../components/header';

class Page1 extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            name: null,
            nameError: null
        }
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        console.log(this.props.user)
        this.didBlurSubscription = this.props.navigation.addListener('didBlur', () => {
            this.setState({ name: null });
        });
    }

    validateNameField(value) {
        const regExp = /[0123456789!@#$%^&*()_+=\[\]{};':"\\|,.<>\/?]/;
        const r = regExp.test(value);

        if (r) {
            return 'Name should contain only letters';
        }
        return null;
    }
    onNameChange = (name) => {
        this.setState({
            name: name,
            nameError: this.validateNameField(name)
        });
    };

    onSubmit() {
        this.props.submitPartialForm({ res: { page: 'Page1', name: this.state.name }, navigation: this.props.navigation });
    }

    render() {
        const { progress, loading } = this.props;
        return (
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null}>
                <AppHeader navigation={this.props.navigation} />
                <View style={{ paddingTop: 25 }}>
                    <ProgressStatus values={['Page1', 'Page2', 'Page3', 'Page4']} status={progress} />
                </View>
                <ScrollView contentContainerStyle={styles.inner} keyboardShouldPersistTaps="handled">
                    <Text style={styles.questionText}>{'What is your name?'}</Text>
                    <View style={styles.textInputWithInfoView}>
                        <TextInputWithInfo
                            onChangeText={this.onNameChange}
                            val={this.state.name}
                            errorVal={this.state.nameError}
                            textAlign={'center'}
                            autoCorrect={false}
                            autoFocus={true}
                        />
                    </View>
                </ScrollView>
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                        disabled={isEmpty(this.state.name) || !isEmpty(this.state.nameError)}
                        onPress={this.onSubmit}
                        style={[styles.submitButton, { backgroundColor: (isEmpty(this.state.name) || !isEmpty(this.state.nameError)) ? Colors.gray : Colors.brandPrimary }]}>
                        {loading ? (
                            <ActivityIndicator color={Colors.white} />
                        ) : (
                            <Text style={styles.submitButtonText}>Next</Text>
                        )}
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Page1;