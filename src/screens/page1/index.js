import Page1 from './page1';
import { connect } from 'react-redux';
import { submitPartialForm } from '../../redux/actions/form';

const mapStateToProps = state => {
	return {
		loading: state.user.loading,
		progress: state.form.progress
	};
}

const mapDispatchToProps = dispatch => {
    return {
		submitPartialForm: (data) => dispatch(submitPartialForm(data))
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Page1);